# -*- coding: utf-8 -*-
"""447 week 7.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1CwrM5zF9BeThpzPH0gcimBp9EHvrZ2OS
"""

#1
country_list=["india","canada","newyork"]
def capitals(x):
  return x.upper()
print(list(map(capitals,country_list)))

#2
country_list=["india","canada","newyork"]
def len_country_six(x):
  return len(x)==6
print(list(filter(len_country_six,country_list)))

#3
countries=["india","usa","greenland","south africa","london","china"]
dictionary = {}
def dict(country):
  for i in countries:
    if i[0] in dictionary:
      dictionary[i[0]]+=1
    else:
      dictionary[i[0]]=1
  return dictionary
print(dict(countries))

#4
divisible_counts = {digit: len([num for num in range(1, 1001) if num % digit == 0]) for digit in range(3, 10)}
max_digit = max(divisible_counts, key=divisible_counts.get)
print(f"The digit {max_digit} has the most multiples: {divisible_counts[max_digit]}")

