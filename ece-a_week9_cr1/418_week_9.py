# -*- coding: utf-8 -*-
"""447 week 9.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1VUJ4kBs9G5Lmg_CUGp9kHi9A5kl3eech
"""

#Write a class counter with increment and reset methods
 #1
class Counter:
    def __init__(self):
        self.count = 0

    def increment(self):
        self.count += 1

    def reset(self):
        self.count = 0
# Create an instance of the Counter class
counter = Counter()

print(counter.count)  # Output: 0

counter.increment()
counter.increment()

print(counter.count)  # Output: 2

counter.reset()

print(counter.count)  # Output:

#[2] Write a class calculator with all basic arithmetic operations like addition , subtraction , multiplication , diviison
class Calculator:
    def add(self, num1, num2):
        return num1 + num2

    def subtract(self, num1, num2):
        return num1 - num2

    def multiply(self, num1, num2):
        return num1 * num2

    def divide(self, num1, num2):
        if num2 != 0:
            return num1 / num2
        else:
            raise ValueError("Cannot divide by zero.")
calculator = Calculator()

result = calculator.add(5, 3)
print(result)  # Output: 8

result = calculator.subtract(10, 4)
print(result)  # Output: 6

result = calculator.multiply(2, 6)
print(result)  # Output: 12

